<?php
namespace App;

//use Guzzle\Http\Client;
//use Guzzle\Plugin\Oauth\OauthPlugin;

require_once __DIR__ . "/../../vendor/autoload.php";

class IntuitApp
{
    protected $config;
    protected $client;
    protected $oauthPlugin;

    public function __construct(\GuzzleHttp\Client $client)
    {
        $this->client = $client;
    }

//    public function setOauthPlugin(Guzzle\Plugin\Oauth\OauthPlugin $oauthPlugin)
//    {
//        $this->oauthPlugin = $oauthPlugin;
//    }

    private function send($path)
    {
        $response = $this->client->request('GET',INTUITCONFIG::HOST_URL.$path,['cert' => ['../certs/keystore.pem', '123456']]);

        var_dump($response);
        exit;

//        $oauth = new OauthPlugin(array(
//            'consumer_key' => INTUITCONFIG::OAUTH_CONSUMER_KEY,
//            'consumer_secret' => INTUITCONFIG::OAUTH_CONSUMER_SECRET,
//            'token' => 'my_token',
//            'token_secret' => 'my_token_secret'
//        ));
//        $this->client->addSubscriber($oauth);
//
//        $response = $this->client->get($path)->send();

//        print_r($response);
    }

    public function getInstitutions()
    {
        $this->send('/institutions');
    }
}